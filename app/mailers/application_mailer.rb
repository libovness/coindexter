class ApplicationMailer < ActionMailer::Base
  default from: 'do-not-reply@coindexter.io'
  layout 'mailer'

end
